<%@ page import = "biblioteka.model.Knjiga"%>
<%@ page import = "java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sve knjige autora</title>
</head>
<body>

	<% List<Knjiga> knjigeAutora = (List<Knjiga>) session.getAttribute("knjigeAut"); %>
<h1>Knjige</h1>
<table border ="1">
<tr><th>Naslov</th></tr>
<% for (Knjiga k : knjigeAutora) {%>
	<tr><td><%=k.getNaslov()%></td></tr>
<%} %>

</table>

</body>
</html>