<%@ page import = "biblioteka.model.Autor"%>
<%@ page import = "java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Administracija</title>
<script src="jquery-1.11.0.js"></script>
<script type="text/javascript">
	
	function proveraAutor() {
		ime = document.dodajAutoraForma.ime.value; 
		prezime = document.dodajAutoraForma.prezime.value; 
		
		poruka = ""; 
		
		if(ime == "") {
			poruka += "Niste uneli ime autora\n";
			document.getElementById("imeSpan").style.visibility = "visible";
		}
		
		
		if(!ime.match(/^([a-zA-Z0-9]{5,})$/)) {
			poruka += "Morate uneti bar 5 cifara\n";
			document.getElementById("imeSpan").style.visibility = "visible";
		}
		
		if(prezime == "") {
			poruka += "Niste uneli prezime autora\n";
			document.getElementById("prezimeSpan").style.visibility = "visible";
		}
		
		if (poruka == "")
	 	 	return true;

	 	 alert(poruka);
	 	 return false;
	}
	
	function proveraKnjiga() {
		naslov = document.dodajKnjiguForma.naslov.value; 
		
		poruka = ""; 
		
		if(naslov == "") {
			poruka += "Niste uneli naslov knjige\n";
			document.getElementById("naslovSpan").style.visibility = "visible";
		}
		
		if (poruka == "")
	 	 	return true;

	 	 alert(poruka);
	 	 return false;
	}
	
	$(document).ready(function(){
		
		$("#btn").click(function(){
    		$(this).parent().parent().parent().after(" <tr><td colspan='2'><input type = 'text'/></td></tr> ");
    	});
		
	});


</script>

</head>
<body>

<% List<Autor> autori = (List<Autor>) session.getAttribute("autori"); %>

<h2>Dodavanje autora</h2>
<form action  = "DodajAutoraServlet" name="dodajAutoraForma" method = "POST" onSubmit="return proveraAutor()">
<table>
<tr><td>Ime</td><td><input name = "ime"/><span style = "color:red; visibility: hidden" id = "imeSpan"> Popunite polje</span></td></tr>
<tr><td>Prezime</td><td><input name = "prezime"/><span style = "color:red; visibility: hidden" id = "prezimeSpan"> Popunite polje</span></td></tr>
<tr><td><input type = "submit" value = "Submit"/></td></tr>
</table>
</form>


<h2>Dodavanje knjiga</h2>
<form action  = "DodajKnjiguServlet" name="dodajKnjiguForma" method = "POST" onSubmit="return proveraKnjiga()">
<table>
<tr><td>Naslov</td><td><input name = "naslov"/><span style = "color:red; visibility: hidden" id = "naslovSpan"> Popunite polje</span></td>
<tr>
<td>Autor</td>
<td><select name = "autori">
<% for (Autor a : autori) {%>
	<option value = "<%=a.getId()%>"><%= a.getIme( )+ " " +  a.getPrezime() %></option>
<%} %>
</select>
</td>
</tr>
<tr><td><input type = "submit" value = "Submit"/></td></tr>
<tr><td><button type = "button" id = "btn">Dodaj Autora</button></td><tr>
</table>
</form>

<h2>Pretraga knjiga</h2>
<form action  = "PretraziKnjigeSerlvet" method = "GET">
<table>
<tr><td>Naslov knjige</td><td><input name = "naslovKnjige"/></td></tr>
<tr><td><input type = "submit" value = "Submit"/></td></tr>
</table>
</form>

</body>
</html>