<%@ page import = "biblioteka.model.Knjiga"%>
<%@ page import = "java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<% List<Knjiga> knjige = (List<Knjiga>) session.getAttribute("knjige"); %>
<h1>Autori</h1>
<table border ="1">
<tr><th>Naslov</th><th>Autor</th><th>Izmena</th></tr>
<% for (Knjiga k : knjige) {%>
	<tr>
		<td><%=k.getNaslov() %></td><td><%=k.getAutor().getIme() + " " + k.getAutor().getPrezime()%></td>
		<td align="center">
		<form action = "UpdateKnjigaServlet">
			<input type="submit" value="izmeni"/>
			<input type="text" name="noviNaslov"/>
			<input type="hidden" name="id" value="<%=k.getId()%>"/>
		</form>
		</td>
	</tr>
<%} %>

</table>

<br>
<a href="index.html">home</a>


</body>
</html>