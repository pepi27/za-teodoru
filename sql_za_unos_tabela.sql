-- Table: public.marka

-- DROP TABLE public.marka;

CREATE TABLE public.marka
(
    id bigint NOT NULL,
    naziv_marke character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT marka_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.marka
    OWNER to postgres;

-- Table: public.model

-- DROP TABLE public.model;

CREATE TABLE public.model
(
    id bigint NOT NULL,
    naziv_modela character varying(255) COLLATE pg_catalog."default",
    putanja character varying(255) COLLATE pg_catalog."default",
    marka_id bigint,
    CONSTRAINT model_pkey PRIMARY KEY (id),
    CONSTRAINT fk_ch57mrcemy6ybwn4o3m4wb22o FOREIGN KEY (marka_id)
        REFERENCES public.marka (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.model
    OWNER to postgres;

-- Table: public.telefon

-- DROP TABLE public.telefon;

CREATE TABLE public.telefon
(
    id bigint NOT NULL,
    cena double precision,
    datum_unosa timestamp without time zone,
    ip_address character varying(255) COLLATE pg_catalog."default",
    kontakt character varying(255) COLLATE pg_catalog."default",
    opis character varying(255) COLLATE pg_catalog."default",
    stanje character varying(255) COLLATE pg_catalog."default",
    model_id bigint,
    CONSTRAINT telefon_pkey PRIMARY KEY (id),
    CONSTRAINT fk_gf9k7rdrx3cmvx9byxkt1k0r2 FOREIGN KEY (model_id)
        REFERENCES public.model (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.telefon
    OWNER to postgres;