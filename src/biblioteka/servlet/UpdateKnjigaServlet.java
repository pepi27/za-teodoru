package biblioteka.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import biblioteka.dao.KnjigaDAO;

public class UpdateKnjigaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UpdateKnjigaServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id_knjige = Integer.parseInt(request.getParameter("id"));
		String noviNaslov = request.getParameter("noviNaslov");
		KnjigaDAO.update(noviNaslov, id_knjige);
		
		response.sendRedirect("ListaKnjigaServlet");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
}