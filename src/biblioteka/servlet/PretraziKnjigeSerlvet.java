package biblioteka.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import biblioteka.dao.KnjigaDAO;
import biblioteka.model.Knjiga;


public class PretraziKnjigeSerlvet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PretraziKnjigeSerlvet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<Knjiga> knjigePoNaslovu = KnjigaDAO.pretragaPoNaslovu(request.getParameter("naslovKnjige"));
		session.setAttribute("knjigePoNaslovu", knjigePoNaslovu);
		response.sendRedirect("pregledKnjigePoNaslovu.jsp");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}