package biblioteka.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import biblioteka.model.Autor;
import biblioteka.model.Knjiga;
import util.ConnectionManager;

public class KnjigaDAO {
	
	public static List<Knjiga> getAll(){
		
		String query = "select k.id, k.naslov, a.id, a.ime, a.prezime from knjiga k join autor a"
				+ " on k.id_autora = a.id";
		
		Statement stmt;
		List<Knjiga> retVal = null;
		try {
			stmt = ConnectionManager.getConnection().createStatement();
			ResultSet rset = stmt.executeQuery(query);
			retVal = new ArrayList<Knjiga>();
			while (rset.next()) {
				int id = rset.getInt(1);
				String naslov = rset.getString(2);
				
				int idAutora = rset.getInt(3);
				String ime = rset.getString(4);
				String prezime = rset.getString(5);
				Autor a = new Autor(idAutora, ime, prezime);
				Knjiga k = new Knjiga(id, naslov, a);
				retVal.add(k);
			}
			rset.close();
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retVal;
	}

	
	public static boolean insert(String naslov, int autor_id){
		boolean retVal = false; 
		String updateQuery = "insert into knjiga(naslov, id_autora) values(?, ?)";
		try {
			PreparedStatement pst = ConnectionManager.getConnection().prepareStatement(updateQuery);
			pst.setString(1, naslov);
			pst.setInt(2, autor_id);
			if (pst.executeUpdate() == 1)
				retVal = true;
			pst.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return retVal;
	}
	
	public static boolean update(String naslov, int book_id){
		boolean retVal = false; 
		String updateQuery = "update knjiga set naslov=? where id=?";
		try {
			PreparedStatement pst = ConnectionManager.getConnection().prepareStatement(updateQuery);
			pst.setString(1, naslov);
			pst.setInt(2, book_id);
			if (pst.executeUpdate() == 1)
				retVal = true;
			pst.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return retVal;
	}
	
	public static List<Knjiga> knjigeOdredjenogAutora(String idAutora) {
		Statement st;
		List<Knjiga> retVal = new ArrayList<Knjiga>();
		String query = "SELECT * FROM knjiga WHERE id_autora=" + idAutora; 
		
		try {
			st = ConnectionManager.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query); 
			while(rs.next()) {
				String naslov = rs.getString("naslov");
				Knjiga knjiga = new Knjiga(naslov);
				retVal.add(knjiga); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return retVal; 
	}
	
	public static List<Knjiga> pretragaPoNaslovu(String naslovTrazene) {
		List<Knjiga> retVal = new ArrayList<Knjiga>();
		Statement st; 
		String query = "select k.id, k.naslov, a.id, a.ime, a.prezime from biblioteka.knjiga k" 
				+ " join biblioteka.autor a on k.id_autora = a.id" 
				+ " where k.naslov like '%" + naslovTrazene + "%'";

		try {
			st = ConnectionManager.getConnection().createStatement();
			ResultSet rset = st.executeQuery(query); 
			while(rset.next()) {
				int id = rset.getInt(1);
				String naslov = rset.getString(2);
				
				int idAutora = rset.getInt(3);
				String ime = rset.getString(4);
				String prezime = rset.getString(5);
				Autor a = new Autor(idAutora, ime, prezime);
				Knjiga k = new Knjiga(id, naslov, a);
				retVal.add(k);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal; 
	}
}